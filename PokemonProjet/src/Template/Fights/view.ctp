<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<div class="fights view large-9 medium-8 columns content">
    <h3><?= h($fight->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Dresseur') ?></th>
            <td><?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Second Dresseur') ?></th>
            <td><?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winner Dresseur') ?></th>
            <td><?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fight->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fight->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fight->modified) ?></td>
        </tr>
    </table>
</div>
