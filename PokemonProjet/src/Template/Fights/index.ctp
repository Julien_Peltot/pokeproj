<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fights
 */
?>

<div class="fights index large-9 medium-8 columns content">
    <h3><?= __('Fights') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <div class="container">
                <div class="row row-cols-3">       
                    <?php foreach ($fights as $fight): ?>
                        <div class="col">
                            <div class="card" style="width: 18rem;"> 
                                <div class="card-body"> 
                                    <h5 class="card-title"> 
                                        <?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?>
                                        VS
                                        <?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?>
                                    </h5>
                                    <h6 class="card-subtitle mb-2 text-muted">combat numéro <?= $this->Number->format($fight->id) ?></h6>
                                    <p class="card-text">
                                        crée le <?= h($fight->created) ?> <br>
                                        modifié le <?= h($fight->modified) ?> <br><br>
                                        Winner : <?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, 
                                        ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?>
                                    </p>
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $fight->id]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fight->id], 
                                    ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>   
                </div>      
            </div>  
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
