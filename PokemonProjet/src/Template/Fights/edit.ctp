<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<div class="fights form large-9 medium-8 columns content">
    <?= $this->Form->create($fight) ?>
    <fieldset>
        <legend><?= __('Edit Fight') ?></legend>
        <?php
            echo $this->Form->control('first_dresseur_id', ['options' => $firstDresseurs]);
            echo $this->Form->control('second_dresseur_id', ['options' => $secondDresseurs]);
            echo $this->Form->control('winner_dresseur_id', ['options' => $winnerDresseurs]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
