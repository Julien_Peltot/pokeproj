<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<div class="dresseurs form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseur) ?>
    <fieldset>
        <legend><?= __('Edit Dresseur') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
