<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur[]|\Cake\Collection\CollectionInterface $dresseurs
 */
?>

<div class="dresseurs index large-9 medium-8 columns content">
    <h3><?= __('Dresseurs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <div class="container">
                <div class="row row-cols-3">       
                        <?php foreach ($dresseurs as $dresseur): ?>
                            <div class="col">
                                <div class="card" style="width: 18rem;"> 
                                    <div class="card-body"> 
                                        <h5 class="card-title"><?= h($dresseur->first_name) ?> <?= h($dresseur->last_name) ?> </h5>
                                        <h6 class="card-subtitle mb-2 text-muted">id: <?= $this->Number->format($dresseur->id) ?></h6>
                                        <p class="card-text">
                                        crée le <?= h($dresseur->created) ?> <br>
                                        modifié le <?= h($dresseur->modified) ?> <br>
                                        </p>
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $dresseur->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseur->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseur->id], 
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>   
                    </div>      
                </div>  
            </div>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>