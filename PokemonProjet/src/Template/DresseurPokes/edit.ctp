<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke $dresseurPoke
 */
?>

<div class="dresseurPokes form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseurPoke) ?>
    <fieldset>
        <legend><?= __('Edit Dresseur Poke') ?></legend>
        <?php
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('poke_id', ['options' => $pokes]);
            echo $this->Form->control('is_fav');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
