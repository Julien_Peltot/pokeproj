<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke $dresseurPoke
 */
?>
<div class="dresseurPokes view large-9 medium-8 columns content">
    <h3><?= h($dresseurPoke->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseurPoke->has('dresseur') ? $this->Html->link($dresseurPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPoke->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Poke') ?></th>
            <td><?= $dresseurPoke->has('poke') ? $this->Html->link($dresseurPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPoke->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseurPoke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseurPoke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseurPoke->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Fav') ?></th>
            <td><?= $dresseurPoke->is_fav ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
