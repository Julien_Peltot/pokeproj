<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke[]|\Cake\Collection\CollectionInterface $dresseurPokes
 */
?>

<div class="dresseurPokes index large-9 medium-8 columns content">
    <h3><?= __('Dresseur Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <div class="container">
                    <div class="row row-cols-3">       
                            <?php foreach ($dresseurPokes as $dresseurPoke): ?>
                                <div class="col">
                                    <div class="card" style="width: 18rem;"> 
                                        <div class="card-body"> 
                                            <h5 class="card-title"> 
                                                <?= $dresseurPoke->has('dresseur') ? $this->Html->link($dresseurPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPoke->dresseur->id]) : '' ?>
                                                have
                                                <?= $dresseurPoke->has('poke') ? $this->Html->link($dresseurPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPoke->poke->id]) : '' ?> 
                                            </h5>
                                            <h6 class="card-subtitle mb-2 text-muted">id: <?= $this->Number->format($dresseurPoke->id) ?></h6>
                                            <p class="card-text">
                                            crée le <?= h($dresseurPoke->created) ?> <br>
                                            modifié le <?= h($dresseurPoke->modified) ?> <br>
                                            </p>
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPoke->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPoke->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPoke->id], 
                                            ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPoke->id)]) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>   
                        </div>      
                    </div>  
                </div>
            </div>
        </tbody>
    </table>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

