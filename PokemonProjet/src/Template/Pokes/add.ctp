<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<div class="pokes form large-9 medium-8 columns content">
    <?= $this->Form->create($poke) ?>
    <fieldset>
        <legend><?= __('Add Poke') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('pokedex_number');
            echo $this->Form->control('health');
            echo $this->Form->control('attack');
            echo $this->Form->control('defense');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
