<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<div class="pokes view large-9 medium-8 columns content">
    <h3><?= h($poke->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($poke->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($poke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokedex Number') ?></th>
            <td><?= $this->Number->format($poke->pokedex_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Health') ?></th>
            <td><?= $this->Number->format($poke->health) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attack') ?></th>
            <td><?= $this->Number->format($poke->attack) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defense') ?></th>
            <td><?= $this->Number->format($poke->defense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($poke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($poke->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseur Pokes') ?></h4>
        <?php if (!empty($poke->dresseur_pokes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Is Fav') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poke->dresseur_pokes as $dresseurPokes): ?>
            <tr>
                <td><?= h($dresseurPokes->id) ?></td>
                <td><?= h($dresseurPokes->dresseur_id) ?></td>
                <td><?= h($dresseurPokes->poke_id) ?></td>
                <td><?= h($dresseurPokes->created) ?></td>
                <td><?= h($dresseurPokes->modified) ?></td>
                <td><?= h($dresseurPokes->is_fav) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokes', 'action' => 'view', $dresseurPokes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokes', 'action' => 'edit', $dresseurPokes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokes', 'action' => 'delete', $dresseurPokes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
