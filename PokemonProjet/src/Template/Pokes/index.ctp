<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>
<div class="pokes index large-9 medium-8 columns content">
    <h3><?= __('Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <div class="container">
                <div class="row row-cols-3">       
                        <?php foreach ($pokes as $poke): ?>
                            <div class="col">
                                <div class="card" style="width: 18rem;"> 
                                    <div class="card-body"> 
                                        <h5 class="card-title"><?= h($poke->name) ?> </h5>
                                        <h6 class="card-subtitle mb-2 text-muted">id: <?= $this->Number->format($poke->id) ?></h6>
                                        <p class="card-text">
                                        pokedex num <?= $this->Number->format($poke->pokedex_number) ?> <br>
                                        crée le <?= h($poke->created) ?> <br>
                                        modifié le <?= h($poke->modified) ?> <br><br>
                                        STAT : <br>
                                         Health : <?= $this->Number->format($poke->health) ?> <br>
                                         Attack : <?= $this->Number->format($poke->attack) ?> <br>
                                         Defence : <?= $this->Number->format($poke->defense) ?> <br>
                                        </p>
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poke->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $poke->id], 
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>   
                    </div>      
                </div>  
            </div>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>