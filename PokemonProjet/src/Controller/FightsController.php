<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {

            $formData = $this->request->getData();
            $formData['winner_dresseur_id'] = $this->_retrieve_FightWinner($formData);

            $fight = $this->Fights->patchEntity($fight, $formData);
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }

    protected function _retrieve_FightWinner($formData){
        $pkmn1 = 100; $pkmn2 =100;
        while($pkmn1 > 0 or $pkmn1 > 0){
              $pkmn1 -= random_int(1,50); 
              $pkmn2 -= random_int(1,50);
        }
        return $pkmn1 > $pkmn2 ? $formData['first_dresseur_id'] : $formData['second_dresseur_id'];
    }

    /**
     * Edit method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $fight = $this->Fights->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $fight = $this->Fights->patchEntity($fight, $this->request->getData());
    //         if ($this->Fights->save($fight)) {
    //             $this->Flash->success(__('The fight has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The fight could not be saved. Please, try again.'));
    //     }
    //     $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
    //     $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
    //     $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
    //     $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    // }

    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
